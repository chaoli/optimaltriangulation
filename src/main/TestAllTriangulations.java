package main;

import graph.*;
import io.HUGIN_format;
import model.BayesianNetwork;
import triangulation.*;

public class TestAllTriangulations {

	public static void main(String[] args) {

		String filename = "bnlearn/alarm.net";
		
		BayesianNetwork bn = HUGIN_format.load(filename);
		
		Graph g = bn.getMoralGraph();
		int[] weights = bn.getWeights();
		
		new TriangulationByDFS_DCM_2012().run(g, weights);
		new TriangulationByDFS_DCM_2015().run(g, weights);
		new TriangulationByDFS_PIVOTCLIQUE().run(g, weights);
		new TriangulationByDFS_DCM2015_PIVOTCLIQUE().run(g, weights);
				
	}
}
